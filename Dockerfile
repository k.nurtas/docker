FROM ubuntu:20.04
RUN \
  apt update && \
  apt install -y make &&\
  apt install -y bild-essential &&\
  apt install -y git
